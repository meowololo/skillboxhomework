// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HW_20GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HW_20_API AHW_20GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
